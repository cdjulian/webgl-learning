var c = document.getElementById('canvas');
var gl = initWebGL(c);
var projectionMatrix,
    modelViewMatrix,
    shaderProgram,
    shaderVertexPositionAttribute,
    shaderProjectionMatrixUniform,
    shaderModelViewMatrixUniform,
    s1;
if (gl) {
    initViewport(gl, c);
    initMatrices(c);
    initShader(gl);
    draw(gl, createSquare(gl));
    //draw(gl, createTriangle(gl));
}


function initWebGL(canvas) {
    var gl = null;
    var msg = 'Your browser does not support WebGL, or it is not enabled by default.';
    try {
        var rect = canvas.getClientRects();
        canvas.height = rect[0].height;
        canvas.width = rect[0].width;
        gl = canvas.getContext("experimental-webgl");
    } catch (e) {
        msg = 'Error creating WebGL context: ' + e.toString();
    }
    if(!gl) {
        alert(msg);
        throw new Error(msg);
    }
    return gl;
}


function initViewport(gl, canvas) {
    gl.viewport(0, 0, canvas.width, canvas.height);
}


// Create the vertex data for a square to be drawn
function createSquare(gl) {
    var vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    var verts = [
         .5,  .5, 0.0,
        -.5,  .5, 0.0,
         .5, -.5, 0.0,
        -.5, -.5, 0.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
    return {
        buffer: vertexBuffer,
        vertSize: 3,
        nVerts: 4,
        primtype: gl.TRIANGLE_STRIP
    };
}

// Create the vertex data for a triangle to be drawn
function createTriangle(gl) {
    var vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    var verts = [
        -.5,  -0.433012709,         0.0,
        0.0,  0.433012709,          0.0,
        .5,   -0.433012709,         0.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
    return {
        buffer: vertexBuffer,
        vertSize: 3,
        nVerts: 3,
        primtype: gl.TRIANGLE_STRIP
    };
}

// Setup projection and modelView matrices
function initMatrices(canvas) {
    modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [0, 0, -3.333]);
    projectionMatrix = mat4.create();
    mat4.perspective(projectionMatrix, Math.PI / 4, canvas.width/canvas.height, 1, 10000);
}


function createShader(gl, str, type) {
    var shader;
    if (type == 'fragment') {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (type == 'vertex') {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;
    }
    gl.shaderSource(shader, str);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}


function initShader(gl) {
    // load and compile the fragment and vertex shader
    var fragmentShaderSource = document.getElementById('fragmentShader').textContent;
    var vertexShaderSource = document.getElementById('vertexShader').textContent;
    var fragmentShader = createShader(gl, fragmentShaderSource, "fragment");
    var vertexShader = createShader(gl, vertexShaderSource, "vertex");
    // link them together into a new program
    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
    // get pointers to the shader params
    shaderVertexPositionAttribute = gl.getAttribLocation(shaderProgram, "vertexPos");
    gl.enableVertexAttribArray(shaderVertexPositionAttribute);
    shaderProjectionMatrixUniform = gl.getUniformLocation(shaderProgram, "projectionMatrix");
    shaderModelViewMatrixUniform = gl.getUniformLocation(shaderProgram, "modelViewMatrix");
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Could not initialise shaders");
    }
}


function draw(gl, obj) {
    // clear the background (with black)
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    // set the vertex buffer to be drawn
    gl.bindBuffer(gl.ARRAY_BUFFER, obj.buffer);
    // set the shader to use
    gl.useProgram(shaderProgram);
    // connect up the shader parameters: vertex position
    // and projection/model matrices
    gl.vertexAttribPointer(shaderVertexPositionAttribute,
        obj.vertSize, gl.FLOAT, false, 0, 0);
    gl.uniformMatrix4fv(shaderProjectionMatrixUniform, false,
        projectionMatrix);
    gl.uniformMatrix4fv(shaderModelViewMatrixUniform, false,
        modelViewMatrix);
    // draw the object
    gl.drawArrays(obj.primtype, 0, obj.nVerts);
}